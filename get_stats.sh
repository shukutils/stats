#!/bin/bash

# This script is intended to create a summary of system-stats on debian-based systems
SCRIPT_VERSION="1.3"

duc_the_system=1

# Check if argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <output-directory>"
    exit 1
else
    INFODIR="$1/stats"
fi
############################
echo "... general information" #
############################
mkdir -p $INFODIR
echo "SCRIPT-VERSION: $SCRIPT_VERSION" > "$INFODIR/info.md"
echo "LAST-RUN: $(date -I)" >> "$INFODIR/info.md"

###################################
echo "... get installed software" #
###################################
dpkg --list > "$INFODIR/software_installed_packages.txt"
apt-mark showauto > "$INFODIR/software_installed_auto_packages.txt"
apt-cache policy > "$INFODIR/software_package_policy.txt"

# Check if flatpak command exists, list flatpak packages
if command -v flatpak &>/dev/null; then
    flatpak list > "$INFODIR/software_flatpak.txt"
fi

# Check if snap command exists, list snap packages
if command -v snap &>/dev/null; then
    snap list > "$INFODIR/software_snap.txt"
fi

# Check if pip3 command exists, list pip packages
if command -v pip3 &>/dev/null; then
    pip3 freeze > "$INFODIR/software_pip3.txt"
fi

# Check if docker command exists, list docker info
if command -v docker &>/dev/null; then
    docker info > "$INFODIR/software_docker_info.txt"
    docker ps -a > "$INFODIR/software_docker_containers.txt"
    docker volume ls > "$INFODIR/software_docker_volumes.txt"
fi

###################################
echo "... get os-level info" #
###################################

lsb_release -a > "$INFODIR/os_version.txt"

# Service
systemctl list-units --type=service > "$INFODIR/os_services_status.txt"

# Crontab
crontab -l > "$INFODIR/os_cron_jobs.txt"

# Users and groups
cat /etc/passwd > "$INFODIR/os_users.txt"
cat /etc/group >  "$INFODIR/os_groups.txt"

# Network
cat /etc/resolv.conf > "$INFODIR/os_dns_settings.txt"

###################################
echo "... get hardware info" #
###################################

echo "    ... inxi-report"
apt install -y inxi
inxi -v 8 > "$INFODIR/hardware_inxi_report_detailed.txt"
inxi -F > "$INFODIR/hardware_inxi_report.txt"

echo "    ... lshw report"
lshw > "$INFODIR/hardware_lshw_report.txt"
lshw -json > "$INFODIR/hardware_lshw_report.json"
lshw -html > "$INFODIR/hardware_lshw_report.html"

echo "    ... dmidecode"
dmi_decode > "$INFODIR/hardware_dmi_decode.txt"

echo "    ... misc"
# Detailed disk information
lsblk -o NAME,MODEL,TYPE,SIZE,MOUNTPOINT > "$INFODIR/hardware_disk_lsblk_detailed_info.txt"
df -h > "$INFODIR/hardware_df_disk_usage.txt"
lsblk > "$INFODIR/hardware_disk_lsblk_layout.txt"

# Detailed RAM information
cat /proc/meminfo > "$INFODIR/hardware_proc_meminfo.txt"

# General information
cat /proc/cpuinfo > "$INFODIR/hardware_cpu_info.txt"
lshw -class processor > "$INFODIR/hardware_cpu_detailed_info.txt"

# Network information
ip addr > "$INFODIR/hardware_network_interfaces.txt"
ifconfig > "$INFODIR/hardware_network_interfaces_old.txt"

###################################
echo "... duc duc duc" #
###################################

# Check for duc installation
if ! command -v duc &> /dev/null; then
    echo "duc is not installed. Skipping duc indexing."
    exit 1
fi

mkdir -p "$INFODIR/duc"
if [ "$duc_the_system" -eq 1 ]; then
    # Identify mount points of all disks
    mountpoints=$(lsblk -o MOUNTPOINT | grep -v '^$')

    # Loop through each mount point and create a duc database
    for mountpoint in $mountpoints; do
        if [[ "$mountpoint" == /snap* ]]; then
            echo "Skipping $mountpoint (snap mount point)"
            continue
        fi

        if [[ "$mountpoint" == "MOUNTPOINT" ]]; then
            echo "Skipping $mountpoint (docker mount point)"
            continue
        fi

        sanitized_mountpoint=$(echo "$mountpoint" | sed 's/[\/ ]/_/g')
        duc index -x -p --database="$INFODIR/duc/duc_${sanitized_mountpoint}.db" "$mountpoint"
        echo "$mountpoint" > "$INFODIR/duc/duc_${sanitized_mountpoint}.info"
    done
fi