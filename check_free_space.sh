#!/bin/bash

# Configurable parameters
THRESHOLD=85  # Set the disk usage percentage threshold
HEALTHCHECK_URL="https://healthchecks.io/ping/1234567890"  # Set the healthcheck URL
FAIL_URL="$HEALTHCHECK_URL/fail"  # Set the fail healthcheck URL
MOUNT_POINT="/"  # Specify the mount point to check (default is root)

# Get the current disk usage percentage for the specified mount point
DISK_USAGE=$(df "$MOUNT_POINT" | awk 'NR==2 {print $5}' | sed 's/%//')

echo "Current disk usage: $DISK_USAGE%"

# Check if disk usage is below the threshold
if [ "$DISK_USAGE" -lt "$THRESHOLD" ]; then
    echo "Disk usage is under $THRESHOLD%, pinging $HEALTHCHECK_URL..."
    curl -fsS -m 10 --retry 5 -o /dev/null "$HEALTHCHECK_URL"
else
    echo "Disk usage is $DISK_USAGE%, which is above or equal to $THRESHOLD%, pinging $FAIL_URL..."
    curl -fsS -m 10 --retry 5 -o /dev/null "$FAIL_URL"
fi

